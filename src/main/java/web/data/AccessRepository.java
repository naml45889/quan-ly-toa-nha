package web.data;

import java.util.Date;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import web.model.Access;
import web.model.EmployeeCompany;

public interface AccessRepository extends CrudRepository<Access, Integer>{
	//Optional<Access> findById(int id);
	@Query("from Access Where accessCard = :accessCard")
	Iterable<Access> findByAccessCard(String accessCard);	
	@Query("from Access Where Time between :startDate and :endDate")
	Iterable<Access> findAllBetweenDates(@Param("startDate") Date startDate, @Param("endDate") Date endDate);	
}
