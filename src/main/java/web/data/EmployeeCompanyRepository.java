package web.data;

import java.util.Date;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import web.model.Access;
import web.model.EmployeeCompany;

public interface EmployeeCompanyRepository extends CrudRepository<EmployeeCompany, String>{
	@Query("from EmployeeCompany Where Time between :startDate and :endDate")
	Iterable<Access> findAllBetweenDates(@Param("startDate") Date startDate, @Param("endDate") Date endDate);	
} 