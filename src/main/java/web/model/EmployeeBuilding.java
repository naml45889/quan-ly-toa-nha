package web.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
@Data
@RequiredArgsConstructor
@Entity
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
@Table(name="Employee_Building")
public class EmployeeBuilding {
	@Id
	public final int id;
	public final String name;
	public final Date dateOfBirth;
	public final String address;
	public final String phoneNumber;
	public final int ranking;
	public final String position;
	@ManyToMany(targetEntity = Service.class) 
	public final List<Service> services; 
	public final double salary;
}	
