package web.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
@Entity
@NoArgsConstructor(access = AccessLevel.PUBLIC, force = true)
@Table(name="Employee_Company")
public class EmployeeCompany {
	@Id
	public final String accessCard;
	@NotNull
	@Column(unique = true)
	@NotNull
	public final String CMT;
	@NotNull
	public final String name;
	public final Date dateOfBirth;
	public final String phoneNumber; 
	

	@OneToMany(mappedBy="employeeCompany")
    @JsonIgnore
	//@JsonManagedReference
	private final List<Access> accesses;
}
